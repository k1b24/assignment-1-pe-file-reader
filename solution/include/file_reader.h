/*!
\file
\brief Заголовочный файл с описанием функций чтения PE файла
*/

#ifndef SECTION_EXTRACTOR_FILE_READER_H


#include "PEFile.h"
#include <stdio.h>

/// Набор возможных результатов операции чтения
enum read_status {
    READ_OK = 0, ///< Успешная операция чтения
    READ_ERROR = 1, ///< При чтении произошла ошибка
};

/*!
Главная функция для чтения структуры struct PEFile из PE файла
\param[out] peFile Указатель на структуру для заполнения
\param[in] in Исходный PE файл
\return Результат операции чтения enum read_status
*/
enum read_status readPEFile(FILE *in, struct PEFile* peFile);
/*!
Функция для чтения заголовка из PE файла
\param[out] peFile Указатель на структуру для заполнения
\param[in] in Исходный PE файл
\return Результат операции чтения enum read_status
*/
enum read_status readPEHeader(FILE *in, struct PEFile* peFile);
/*!
Главная функция для чтения заголовков секций PE файла
\param[out] peFile Указатель на структуру для заполнения
\param[in] in Исходный PE файл
\return Результат операции чтения enum read_status
*/
enum read_status readSectionHeaders(FILE *in, struct PEFile* peFile);


#define SECTION_EXTRACTOR_FILE_READER_H

#endif //SECTION_EXTRACTOR_FILE_READER_H
