/*!
\file
\brief Заголовочный файл с описанием структуры заголовка секции PE файла
*/

#if defined(__clang__) || defined(__GNUC__)
struct __attribute__((packed)) PESectionHeader {
#elif defined(_MSC_VER)
    #pragma pack(push, 1)
    struct PESectionHeader {
#endif
    /// \brief Название секции
    char name[8];
    /// \brief Виртуальный размер секции, когда она загружена в память
    uint32_t virtual_size;
    /// \brief Виртуальный адрес начала секции, когда она загружена в память
    uint32_t virtual_address;
    /// \brief Размер секции на диске
    uint32_t size_of_row_data;
    /// \brief Указатель на начало секции на диске
    uint32_t pointer_to_row_data;
    /// \brief Указатель файла на начало записей релокации для раздела
    uint32_t pointer_ro_relocations;
    /// \brief УУказатель файла на начало записей номера строки для раздела
    uint32_t pointer_to_linenumbers;
    /// \brief Количество записей о перемещении для раздела
    uint16_t number_of_relocations;
    /// \brief Количество записей номера строки для раздела
    uint16_t number_of_linenumbers;
    /// \brief Флаги, описывающие характеристики раздела
    uint32_t characteristics;
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif
