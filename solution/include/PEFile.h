/*!
\file
\brief Заголовочный файл с описанием структуры PE файла
*/

#ifndef ASSIGNMENT_1_PE_FILE


#include "PEHeader.h"
#include "PESectionHeader.h"
#include <stdint.h>

/*!
\brief Структура PE файла
\note Структура упакована для корректной работы с ней
*/
#if defined(__clang__) || defined(__GNUC__)
struct __attribute__((packed)) PEFile {
#elif defined(_MSC_VER)
#pragma pack(push, 1)
    struct PEFile {
#endif
        /// \brief Указатель на PE Header
    uint32_t magic_offset;
        /// \brief Указатель на начало PE Header
    uint32_t header_offset;
        /// \brief Указатель на начало опционального заголовка PE файла
    uint32_t optional_header_offset;
        /// \brief Указатель на начало заголовков секций PE файлов
    uint32_t section_header_offset;

        /// \brief Заголовок PE файла
    struct PEHeader header;
        /// \brief Массив заголовков секций
    struct PESectionHeader *section_headers;

};

#ifdef _MSC_VER
#pragma pack(pop)
#endif

#define ASSIGNMENT_1_PE_FILE

#endif
