/*!
\file
\brief Заголовочный файл с описанием структуры заголовка PE файла
*/

#ifndef ASSIGNMENT_1_PE_FILE_READER_PEHEADER_H


#include <stdint.h>


#if defined(__clang__) || defined(__GNUC__)
struct __attribute__((packed)) PEHeader {
#elif defined(_MSC_VER)
#pragma pack(push, 1)
    struct PEHeader {
#endif
    /// \brief Число, определяющее целевую машину
    uint16_t machine;
    /// \brief Число, определяющее количество секций
    uint16_t number_of_sections;
    /// \brief Число, определяющее дату создания файла
    uint32_t time_data_stamp;
    /// \brief Указатель на таблицу символов COFF
    uint32_t pointer_to_symbol_table;
    /// \brief Количество вхождений в таблице символов COFF
    uint32_t number_of_symbols;
    /// \brief Размер опциональных заголовков PE файла
    uint16_t size_of_optional_header;
    /// \brief Флаги задающие параметры атрибутов
    uint16_t characteristics;
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif


#define ASSIGNMENT_1_PE_FILE_READER_PEHEADER_H

#endif //ASSIGNMENT_1_PE_FILE_READER_PEHEADER_H
