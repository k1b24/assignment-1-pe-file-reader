/// @file
/// @brief Главный файл приложения

#include "file_reader.h"
#include "section_finder.h"
#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Вывести информацию об использовании программы
/// @param[in] f Файл для вывода информации (прим.: stdout)
void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Точка входа в программу
/// @param[in] argc Количество аргументов командной строки
/// @param[in] argv Аргументы командной строки
/// @return 0 при успехе или код ошибки
int main(int argc, char **argv) {
    if (argc != 4) {
        usage(stdin);
        return -1;
    }
    FILE* file_from = fopen(argv[1], "rb");
    if (!file_from) {
        return -1;
    }
    struct PEFile* peFile = malloc(sizeof(struct PEFile));
    if (!peFile) {
        return -1;
    }
    enum read_status read_result = readPEFile(file_from, peFile);
    if (read_result == READ_ERROR) {
        free(peFile);
        return -1;
    }
    FILE* file_out = fopen(argv[3], "wb");
    if (!file_out) {
        free(peFile->section_headers);
        free(peFile);
        return -1;
    }
    read_result = find_section(file_from, file_out, peFile, argv[2]);
    if (read_result == READ_ERROR) {
        free(peFile->section_headers);
        free(peFile);
        return -1;
    }
    free(peFile->section_headers);
    free(peFile);
    return 0;
}
