/*!
\file
\brief Файл с реализацией поиска секции PE файла
*/

#include "section_finder.h"
#include <stdlib.h>
#include <string.h>

/*!
Функция для нахождения секции и записи ее в выходной файл
\param[in] in Исходный PE файл
\param[in] out Выходной файл
\param[in] peFile Структура, описывающая PE файл
\param[in] section_name Название секции для поиска
\return Результат операции чтения
*/
int find_section(FILE *in, FILE* out, struct PEFile* peFile, char* section_name) {
    for (int i = 0; i < peFile->header.number_of_sections; i++) {
        if (strcmp(((struct PESectionHeader*) peFile->section_headers)[i].name, section_name) == 0) {
            uint32_t size_of_raw_data = ((struct PESectionHeader*) peFile->section_headers)[i].size_of_row_data;
            uint32_t pointer_to_row_data = ((struct PESectionHeader*) peFile->section_headers)[i].pointer_to_row_data;
            fseek(in, pointer_to_row_data, SEEK_SET);
            uint8_t* section_data = malloc(sizeof(uint8_t) * size_of_raw_data);
            fread(section_data, sizeof(uint8_t), size_of_raw_data, in);
            fwrite(section_data, sizeof(uint8_t), size_of_raw_data, out);
            free(section_data);
            return 0;
        }
    }
    return 1;
}
