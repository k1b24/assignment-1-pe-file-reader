/*!
\file
\brief Файл с реализацией функций чтения PE файла
*/

#include <file_reader.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IMAGE_FILE_SIGNATURE 0x5A4D
#define OFFSET_TO_EFFECTIVE_OFFSET 0x3C
#define PE_HEADER_SIGNATURE_SIZE 4
#define CORRECT_PE_HEADER_SIGNATURE "PE\0\0"

/*!
Главная функция для чтения структуры struct PEFile из PE файла
\param[out] peFile Указатель на структуру для заполнения
\param[in] in Исходный PE файл
\return Результат операции чтения enum read_status
*/
enum read_status readPEFile(FILE* in, struct PEFile* pefile) {
    uint16_t file_signature;
    if (!fread(&file_signature, sizeof(uint16_t), 1, in)) {
        return READ_ERROR;
    }
    if (file_signature != IMAGE_FILE_SIGNATURE) {
        return READ_ERROR;
    }
    if (fseek(in, OFFSET_TO_EFFECTIVE_OFFSET, SEEK_SET)) {
        return READ_ERROR;
    }
    if (!fread(&pefile->magic_offset, sizeof(uint32_t), 1, in)) {
        return READ_ERROR;
    }
    pefile->header_offset = pefile->magic_offset + PE_HEADER_SIGNATURE_SIZE;
    if(fseek(in, pefile->magic_offset, SEEK_SET)) {
        return READ_ERROR;
    }
    char pe_signature[4];
    if(!fread(&pe_signature, sizeof(char), PE_HEADER_SIGNATURE_SIZE, in)) {
        return READ_ERROR;
    }
    if (strcmp(pe_signature, CORRECT_PE_HEADER_SIGNATURE) != 0) {
        return READ_ERROR;
    }
    pefile->optional_header_offset = pefile->header_offset + sizeof(struct PEHeader);
    enum read_status read_result = readPEHeader(in, pefile);
    if (read_result == READ_ERROR) {
        return READ_ERROR;
    }
    pefile->section_headers = malloc(sizeof(struct PESectionHeader) * pefile->header.number_of_sections);
    if (!pefile->section_headers) {
        return READ_ERROR;
    }
    read_result = readSectionHeaders(in, pefile);
    if (read_result == READ_ERROR) {
        free(pefile->section_headers);
        return READ_ERROR;
    }
    return READ_OK;
}

/*!
Функция для чтения заголовка из PE файла
\param[out] peFile Указатель на структуру для заполнения
\param[in] in Исходный PE файл
\return Результат операции чтения enum read_status
*/
enum read_status readPEHeader(FILE *in, struct PEFile* peFile) {
    if(fseek(in, peFile->header_offset, SEEK_SET)) {
        return READ_ERROR;
    }
    if (!fread(&peFile->header, sizeof(struct PEHeader), 1, in)) {
        return READ_ERROR;
    }
    peFile->section_header_offset = peFile->optional_header_offset + peFile->header.size_of_optional_header;
    return READ_OK;
}

/*!
Главная функция для чтения заголовков секций PE файла
\param[out] peFile Указатель на структуру для заполнения
\param[in] in Исходный PE файл
\return Результат операции чтения enum read_status
*/
enum read_status readSectionHeaders(FILE *in, struct PEFile* peFile) {
    if (fseek(in, peFile->section_header_offset, SEEK_SET)) {
        return READ_ERROR;
    }
    if (!fread(peFile->section_headers, sizeof(struct PESectionHeader), peFile->header.number_of_sections, in)) {
        return READ_ERROR;
    }
    return READ_OK;
}
