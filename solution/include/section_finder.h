/*!
\file
\brief Заголовочный файл с описанием функции нахождения определенной секции PE файла
*/

#ifndef ASSIGNMENT_1_PE_FILE_READER_SECTION_FINDER_H


#include "PEFile.h"
#include <stdio.h>
/*!
Функция для нахождения секции и записи ее в выходной файл
\param[in] in Исходный PE файл
\param[in] out Выходной файл
\param[in] peFile Структура, описывающая PE файл
\param[in] section_name Название секции для поиска
\return Результат операции чтения
*/
int find_section(FILE *in, FILE* out, struct PEFile* peFile, char* section_name);


#define ASSIGNMENT_1_PE_FILE_READER_SECTION_FINDER_H

#endif //ASSIGNMENT_1_PE_FILE_READER_SECTION_FINDER_H
